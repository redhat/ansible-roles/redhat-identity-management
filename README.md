# Red Hat Identity Management

THIS IS STILL A WORK IN PROGRESS; NEEDS MORE TESTING.

This is an example playbook / role that uses the official ansible-freeipa roles ipaserver, ipareplica and ipaclient.

It is setup a bit weirdly since the structure of the ansible-freeipa is not made as a role, but rather a collection. So we need to be a bit inventive to subtract the actual roles so we can make use of them.

Similar to ansible-freeipa this will only execute on hosts that inside specific groups, namely:
ipaserver, ipareplica and ipaclients

You can find an example of how to use it in the example-playbooks folder. 

Essentially, include it as a role, if the host exists in either of the host groups, it will apply the applicable role to it.

You can use the variables for the roles to configure the defaults to what you want them to be.

